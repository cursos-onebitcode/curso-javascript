import calculate from "../../JS-IV/aula-19-modularizacao-calcjs/js/calculate.js"
import copyToClipboard from "../../JS-IV/aula-19-modularizacao-calcjs/js/copyToClipboard.js"
import { clear, handleButtonPress, handleTyping } from "../../JS-IV/aula-19-modularizacao-calcjs/js/keyHandlers.js"
import themeSwitcher from "../../JS-IV/aula-19-modularizacao-calcjs/js/themeSwitcher.js"

document.querySelectorAll('.charKey').forEach((button) => {
  button.addEventListener('click', handleButtonPress)
})

document.getElementById('clear').addEventListener('click', clear)

// Evento keydown, evento de tecla precionada
document.getElementById('input').addEventListener('keydown', handleTyping);

document.getElementById('equal').addEventListener('click', calculate);

document.getElementById('copyToClipboard').addEventListener('click', copyToClipboard)

document.getElementById('themeSwitcher').addEventListener('click', themeSwitcher)