function addPlayer() {
  const posicao = document.getElementById('position').value;
  const nome = document.getElementById('name').value;
  const numero = document.getElementById('number').value

  const confirmar = confirm(`Confirma a escalação do jogador ${nome}, na posicão ${posicao} de número ${numero}`);

  if (confirmar) {
    const teamList = document.getElementById('team-list');
    const player = document.createElement('li');
    player.id = "player-" + numero;
    player.innerText = `Jogador: ${nome}, posição: ${posicao}, número: ${numero}`;
    teamList.appendChild(player)

    document.getElementById('position').value = "";
    document.getElementById('name').value = "";
    document.getElementById('number').value = "";
  }
}

function removePlayer() {
  const numberToRemove = document.getElementById('numberToRemove').value;
  const jogadorEliminado = document.getElementById('player-' + numberToRemove);

  const confirmar = confirm(`Deseja mesmo remover o jogador de numero ${numberToRemove}?`)

  if (confirmar) {
    document.getElementById('team-list').removeChild(jogadorEliminado)
    // jogadorEliminado.remove();
    document.getElementById('numberToRemove').value = "";
  }
}