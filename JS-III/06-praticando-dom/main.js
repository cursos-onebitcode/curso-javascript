function addContact() {
  const contactsList = document.getElementById('contacts-list');
  
  const h3 = document.createElement('h3');
  h3.innerText = 'Contato';

  const ul = document.createElement('ul')

  const nameLi = document.createElement('li')
  nameLi.innerText = 'Nome: '
  const nameInput = document.createElement('input')
  nameInput.type = 'text';
  nameInput.name = 'fullname'
  nameLi.appendChild(nameInput)
  ul.appendChild(nameLi)

  const phoneLi = document.createElement('li')
  phoneLi.innerText = 'Telefone: '
  const phoneInput = document.createElement('input')
  phoneInput.type = 'text';
  phoneInput.name = 'phone'
  phoneLi.appendChild(phoneInput)
  ul.appendChild(phoneLi)
  const addressLi = document.createElement('li')
  addressLi.innerHTML = '<label for="address">Endereço: </label>';

  const addressinput = document.createElement('input')
  addressinput.type = 'text';
  addressinput.name = 'address';
  addressinput.id = 'address'
  addressLi.appendChild(addressinput)
  ul.appendChild(addressLi)
  ul.appendChild(document.createElement('br'));
  contactsList.append(h3, ul)
}

function removeContact() {
  const contactsList = document.getElementById('contacts-list');

  const titles = document.getElementsByTagName('h3')
  const contacts = document.getElementsByTagName('ul')

  contactsList.removeChild(titles[titles.length - 1]);
  contactsList.removeChild(contacts[contacts.length - 1]);
}