// Session Storage
document.getElementById('sessionBtn').addEventListener('click', () => {
  const input = document.getElementById('session')
  //Salvar informacoes no session storage
  sessionStorage.setItem('info', input.value)
  input.value = '';
});

//Ler informações da session
document.getElementById('readSession').addEventListener('click', () => {
  const info = sessionStorage.getItem('info')
  alert(`A informação salva é ${info}`)
})

// Local Storage
document.getElementById('localBtn').addEventListener('click', () => {
  const input = document.getElementById('local')
  //Salvar informacoes no local storage
  localStorage.setItem('local', input.value)
})

//Ler informações da local
document.getElementById('readLocal').addEventListener('click', () => {
  const local = localStorage.getItem('local')
  alert(`A informação salva do local é ${local}`)
})

// Cookies
document.getElementById('cookieBtn').addEventListener('click', function () {
  const input = document.getElementById('cookie')
  // cookieName=value; expires=UTCStringDate; path=/;
  const cookie = 'info=' + input.value + ';'
  const expiration = 'expires=' + new Date(2025, 9, 9) + ';'
  const path = 'path=/;'
  document.cookie = cookie + expiration + path
  input.value = ''
  console.log(document.cookie)
})

document.getElementById('cookie2Btn').addEventListener('click', function () {
  const input = document.getElementById('cookie2')
  // cookieName=value; expires=UTCStringDate; path=/;
  const cookie = 'text=' + input.value + ';'
  const expiration = 'expires=' + new Date(2025, 8, 9) + ';'
  const path = 'path=/;'
  document.cookie = cookie + expiration + path
  input.value = ''
  console.log(document.cookie)
})

