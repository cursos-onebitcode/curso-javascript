const dayjs = require("dayjs");

//Importando dayjs
function exercicioData(dataNascimento) {
    const aniversario = dayjs(dataNascimento);
    const diaAtual = dayjs() // Pega o dia de hoje
    const idade = diaAtual.diff(dataNascimento, 'year')
    const proximoAniversario = aniversario.add(idade + 1, 'year')
    const diasParaProximoAniversario = proximoAniversario.diff(diaAtual, 'day') + 1

    console.log(`
    Idade: ${idade},
    Dia de nascimento: ${aniversario.format("DD/MM/YYYY")},
    Próximo aniversário: ${proximoAniversario.format("DD/MM/YYYY")},
    Dias para o próximo aniversário: ${diasParaProximoAniversario} dias
    `);
}

exercicioData('2002-04-11')



