const path = require("path")
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: {
    index: './src/index.js',
    //mais de um arquivo de entrada
    // hello: './src/hello.js'
  },
  mode: 'development', //production, development
  //saida
  // output: {
  //   path: path.resolve(__dirname, 'public'), // pega o caminho completo da aplicação ex: D:\git-estudos\Onebitcode\curso-javascript\JS-IV\aula-27-usando-o-webpack\public
  //   filename: '[name].bundle.prod.min.js' //mais de um arquivo de entrada
  // }
  module: {
    rules: [{
      test: /\.css$/, //em quais arquivos que vai aplicar o loader, utilizando expressões regulares
      use: [MiniCssExtractPlugin.loader, 'css-loader']
    }]
  },
  plugins: [
    new MiniCssExtractPlugin()
  ]
}