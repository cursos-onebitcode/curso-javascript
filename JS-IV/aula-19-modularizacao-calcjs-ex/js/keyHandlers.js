import calculate from "./calculate.js"

const input = document.getElementById('input')
const allowedKeys = ["(", ")", "/", "*", "-", "+", "9", "8", "7", "6", "5", "4", "3", "2", "1", "0", ".", "%", " "]

export function handleButtonPress(e) {
  const button = e.currentTarget
  const value = button.dataset.value
  input.value += value
}

export function clear() {
  input.value = '';
  input.focus(); //seleciona o input
}

export function handleTyping(e) {
  e.preventDefault();
  if (allowedKeys.includes(e.key)) {
    input.value += e.key
  }
  if (e.key === 'Backspace') {
    input.value = input.value.slice(0, -1)
  }
  if (e.key === 'Enter') {
    calculate();
  }
}