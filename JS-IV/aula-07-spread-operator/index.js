// Spread -> espalha elementos de um objeto iterável

// Passar parametros de uma vez, clonar objetos iteraveis, realizacao de manipulacoes, strings em arrays


const towns = ['Prontera', 'Izlude', 'Payon', 'Alberta', 'Geffen', 'Morroc']

console.log(towns);
console.log(...towns);
console.log(...towns[0]);

const townsCopy = towns

townsCopy.pop() //Remove o ultimo elemento
townsCopy.pop()
townsCopy.push('Juno')

console.log({ towns, townsCopy });

const townsClone = [...towns] //Sem interferencia no array de origem
townsClone.push('Eu')
console.log(
  { towns, townsCopy, townsClone }
);

const townsObject = { ...towns }
const townsObjectClone = { ...townsObject }
townsObjectClone.test = 'Teste'
console.log({ townsObject, townsObjectClone });