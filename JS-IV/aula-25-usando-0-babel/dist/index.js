"use strict";

// Media
var avarege = function avarege() {
  for (var _len = arguments.length, numbers = new Array(_len), _key = 0; _key < _len; _key++) {
    numbers[_key] = arguments[_key];
  }
  var sum = numbers.reduce(function (accum, num) {
    return accum + num;
  }, 0);
  return sum / numbers.length;
};
console.log("A m\xE9dia \xE9 ".concat(avarege(10, 10, 10, 10)));

// Media ponderada
var mediaPonderada = function mediaPonderada() {
  for (var _len2 = arguments.length, entradas = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    entradas[_key2] = arguments[_key2];
  }
  var sum = entradas.reduce(function (accum, _ref) {
    var numero = _ref.numero,
      peso = _ref.peso;
    return accum + numero * (peso !== null && peso !== void 0 ? peso : 1);
  }, 0);
  var sumPeso = entradas.reduce(function (accum, entrada) {
    var _entrada$peso;
    return accum + ((_entrada$peso = entrada.peso) !== null && _entrada$peso !== void 0 ? _entrada$peso : 1);
  }, 0);
  return sum / sumPeso;
};
console.log("M\xE9dia Ponderada: ".concat(mediaPonderada({
  numero: 9,
  peso: 3
}, {
  numero: 7
},
// (entrada.peso ?? 1) -> 1
{
  numero: 10,
  peso: 1
})));

// Mediana
var median = function median() {
  for (var _len3 = arguments.length, numbers = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    numbers[_key3] = arguments[_key3];
  }
  var orderedNumbers = [].concat(numbers).sort(function (a, b) {
    return a - b;
  });
  var middle = Math.floor(orderedNumbers.length / 2);
  if (orderedNumbers.length % 2 !== 0) {
    return orderedNumbers[middle];
  }
  var firstMedian = orderedNumbers[middle - 1];
  var secondMedian = orderedNumbers[middle];
  return avarege(firstMedian, secondMedian);
};
console.log("Mediana: ".concat(median(2, 5, 99, 4, 42, 7)));
console.log("Mediana: ".concat(median(15, 14, 8, 7, 3)));

// Moda
var mode = function mode() {
  for (var _len4 = arguments.length, numbers = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
    numbers[_key4] = arguments[_key4];
  }
  // [ [n, qntd], [n, qntd] ]
  var quantities = numbers.map(function (num) {
    return [num, numbers.filter(function (n) {
      return num === n;
    }).length];
  });
  quantities.sort(function (a, b) {
    return b[1] - a[1];
  });
  return quantities[0][0];
};
console.log("Moda: ".concat(mode(1, 1, 99, 99, 99, 99, 99, 99, 99, 99, 5, 4, 9, 7, 4, 3, 5, 2, 4, 0, 4)));