// Media
const avarege = (...numbers) => {
  const sum = numbers.reduce((accum, num) => accum + num, 0)
  return sum / numbers.length
}

console.log(`A média é ${avarege(10, 10, 10, 10)}`);

// Media ponderada
const mediaPonderada = (...entradas) => {
  const sum = entradas.reduce((accum, { numero, peso }) => accum + (numero * (peso ?? 1)), 0)
  const sumPeso = entradas.reduce((accum, entrada) => accum + (entrada.peso ?? 1), 0)
  return sum / sumPeso
}

console.log(`Média Ponderada: ${mediaPonderada(
  { numero: 9, peso: 3 },
  { numero: 7 }, // (entrada.peso ?? 1) -> 1
  { numero: 10, peso: 1 },
)}`)

// Mediana
const median = (...numbers) => {
  const orderedNumbers = [...numbers].sort((a, b) => a - b)
  const middle = Math.floor(orderedNumbers.length / 2)
  if (orderedNumbers.length % 2 !== 0) {
    return orderedNumbers[middle]
  }
  const firstMedian = orderedNumbers[middle - 1]
  const secondMedian = orderedNumbers[middle]
  return avarege(firstMedian, secondMedian)
}

console.log(`Mediana: ${median(2, 5, 99, 4, 42, 7)}`)
console.log(`Mediana: ${median(15, 14, 8, 7, 3)}`)

// Moda
const mode = (...numbers) => {
  // [ [n, qntd], [n, qntd] ]
  const quantities = numbers.map(num => [
    num,
    numbers.filter(n => num === n).length
  ])
  quantities.sort((a, b) => b[1] - a[1])
  return quantities[0][0]
}

console.log(`Moda: ${mode(1, 1, 99, 99, 99, 99, 99, 99, 99, 99, 5, 4, 9, 7, 4, 3, 5, 2, 4, 0, 4)}`) 