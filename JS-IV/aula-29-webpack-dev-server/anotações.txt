------------------------------------------------------------------------------------------------------------------------------------------------------------|

Webpack dev server:
    - Pacote utilizado em conjunto com o Webpack, que permite criar um servidor de desenvolvimento para rodar as aplicações enquanto trabalhamos nela,
    - Olha a aula 28, aonde sera mostrado isso
    - Ele faz o empacotamento automaticamente

------------------------------------------------------------------------------------------------------------------------------------------------------------|

Para instalar basta executar:
    - npm i -D webpack-dev-server

------------------------------------------------------------------------------------------------------------------------------------------------------------|

Configurando o webpack no próprio arquivo de configuracao do webpack:
    -devServer: {
    static: {
      directory: path.resolve(__dirname, 'dist') //caminho dos arquivos staticos
    }
  }

------------------------------------------------------------------------------------------------------------------------------------------------------------|

Para usar basta digitar:
    - npx webpack-dev-server