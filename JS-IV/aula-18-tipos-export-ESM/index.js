import { inline } from "./inline.js"
import defaultInline from "./inline.js"
import exportDefault, { group } from "./non-line.js"

inline()
defaultInline()

group()
exportDefault()