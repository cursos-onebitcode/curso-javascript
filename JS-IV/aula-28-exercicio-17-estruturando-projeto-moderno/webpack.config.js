const path = require("path")

module.exports = {
  devServer: {
    static: {
      directory: path.resolve(__dirname, 'dist'), //caminho dos arquivos staticos
    },
    // compress: true, //compactar arquivo zip
    // port: 8000 //muda de porta
  },
  mode: 'production',
  entry: {
    index: './src/index.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/, //em quais arquivos que vai aplicar o loader, utilizando expressões regulares
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(?:js|mjs|cjs)$/,
        use: ['babel-loader']
      }
    ]
  },
  output: {
    // path: path.resolve(__dirname, 'public'),
    filename: '[name].min.js'
  }
}