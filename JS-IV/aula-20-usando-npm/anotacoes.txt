Inicialização de projeto:
    - npm init 
    - package.json -> gerencia o projeto e suas dependencias
    - package-lock.json -> faz uma trava nas versoes que esta utilizando
    - node_modules -> onde fica todos os pacotes que foi instalado.

Instalando um pacote:
    - npm install --save (nome-pacote)

Removendo um pacote:
    - npm uninstall (nome-pacote)

dependencias de desenvolvimento (dev), são aquelas que precisamos quando estamos desenvolvendo a aplicação:
    - npm install --save-dev (nome-pacote)

instalação de dependencias globais (no computador):
    - npm install --global (nome-pacote)