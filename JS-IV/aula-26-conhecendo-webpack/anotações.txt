Muito utilizada no front-end

-----------------------------------------------------------------------------|
O que é o webpack?
    - Empacotador de módulo estático
    - Empacotar todos os módulos de uma aplicação a partir de um ou mais pontos de entrada em um ou mais bundles
    - Constroi um grafo de dependencias para pontos de entrada, pegando todos os módulos que são necessários 

-----------------------------------------------------------------------------|

Peças chave do webpack:
    - Entry -> Pontos de entrada de onde o webpack procurará por arquivos para empacotar
    - Output -> Saída resultante do processo de emapacotamento
    - Loaders -> Por padrão ele entende JSON e javascript
                 - Loaders permitem que o webpack consiga entender outros tipos de arquivo
    - PLugins -> Estenções que são utilizados para realizar tarefas mais elaboradas.

-----------------------------------------------------------------------------|
Porque utilizar o webpack?
    - Possibilidade de trabalhar com módulos de forma fácil, inclusive com COMMON JSON
    - Possibilidade de automatizar o gerenciamento de módulos e dependencias da aplicação
    - Empacotar os módulos em arquivos estáticos permite que estles sejam servidos na web
    - Uma das soluções mais utilizadas do mercado para gerenciamento de assets estáticos,
      utilizados por grandes empresas e frameworks como next.js, vue.js