let opcao = "";

do {
  opcao = prompt(
    "1- Para calcular a área de um triangulo\n" +
    "2- Para calcular a área de um retangulo\n" +
    "3- Para calcular a área de um quadrado\n" +
    "4- Para calcular a área de um trapezio\n" +
    "5- Para calcular a área de um circulo\n" +
    "6- Sair\n"
  );
  switch (opcao) {
    case "1":
      let base = parseFloat(prompt("Insira o valor da base..."));
      let alturaT = parseFloat(prompt("Insira o valor da altura..."));
      alert(`O valor da área do triangulo é ${areaTriangulo(base, alturaT)}`);
      break;
    case "2":
      let baseR = parseFloat(prompt("Insira o valor da base..."));
      let alturaR = parseFloat(prompt("Insira o valor da altura..."));
      alert(`O valor da área do retangulo é ${areaRetangulo(baseR, alturaR)}`);
      break;
    case "3":
      let lado = parseFloat(prompt("Insira o valor do lado..."));
      alert(`O valor da área do quadrado é ${areaQuadrado(lado)}`);
      break;
    case "4":
      let baseMaior = parseFloat(prompt("Insira o valor da base maior..."));
      let baseMenor = parseFloat(prompt("Insira o valor da base menor..."));
      let altura = parseFloat(prompt("Insira o valor da altura..."));
      alert(`O valor da área do trapézio é ${areaTrapezio(baseMaior, baseMenor, altura)}`);
      break;
    case "5":
      let raio = parseFloat(prompt("Insira o valor do raio..."));
      alert(`O valor da área do circulo é ${areaCirculo(raio)}`);
      break;
    case "6":
      alert("Saindo....");
      break;
    default:
      alert("Informe uma opcao válida");
      break;
  }
} while (opcao !== "6");

function areaTriangulo(base, altura) {
  return (base * altura) / 2;
}

function areaRetangulo(base, altura) {
  return base * altura;
}

function areaQuadrado(lado) {
  return (lado * lado);
}

function areaTrapezio(baseMaior, baseMenor, altura) {
  return (baseMaior + baseMenor) * altura / 2;
}

function areaCirculo(raio) {
  const pi = 3.14;
  return pi * raio * raio
}