// Escreva um programa em javascript para simular uma fila de espera em um consultório médico. 
// O programa deve iniciar mostrando na tela um menu interativo contendo a lista de todos os pacientes esperando em ordem mostrando sua posição ao lado do nome 
// (ex.: 1º Matheus, 2º Marcos, etc). O menu também deve permitir escolher entre as opções de “Novo paciente”, para adicionar um novo paciente ao fim da fila 
// (pedindo o nome do paciente), “Consultar paciente”, que retira o primeiro paciente da fila e mostra na tela o nome do paciente consultado, e “Sair”. 
// O programa só deve ser encerrado ao escolher a opção de “Sair”, caso contrário deve voltar ao menu.

// Menu interativo
// - Listagem de pacientes com sua posicao
// - Novo paciente
// - Adiconar novo paciente (final da fila) pedindo o nome dele
// - Consultar paciente, retira o paciente da fila e mostra o nome dele
// - Sair

let fila = [];
let opcao = "";

do {
  let pacientes = "";

  for (let index = 0; index < fila.length; index++) {
    pacientes += (index + 1) + "º  - " + fila[index] + "\n";
  }

  opcao = prompt(`Pacientes: \n ${pacientes} \n 1 - Novo Paciente \n 2 - Consultar paciente \n 3 - Sair`)

  switch (opcao) {
    case "1":
      const novoPaciente = prompt("Insira o nome do paciente")
      fila.push(novoPaciente)
      break;
    case "2":
      const pacienteConsultado = fila.shift();
      if (!pacienteConsultado) {
        alert("Não Existem pacientes para serem consultados")
      } else {
        alert(`O paciente ${pacienteConsultado} foi para consulta e foi removido da lista de espera`)
      }
      break;
    case "3":
      alert("Saindo");
      break;
    default:
      alert("Insira uma opção válida");
  }
} while (opcao !== "3");

// const baralho = []
// let opcao = ""

// do {
//   opcao = prompt(
//     "Cartas no baralho: " + baralho.length +
//     "\n1. Adicionar uma carta\n2. Puxar uma carta\n3. Sair"
//   )

//   switch (opcao) {
//     case "1":
//       const novaCarta = prompt("Qual é a carta?")
//       baralho.push(novaCarta)
//       break
//     case "2":
//       const cartaPuxada = baralho.pop()
//       if (!cartaPuxada) {
//         alert("Não há nenhuma carta no baralho!")
//       } else {
//         alert("Você puxou um(a) " + cartaPuxada)
//       }
//       break
//     case "3":
//       alert("Saindo...")
//       break
//     default:
//       alert("Opção inválida!")
//   }

// } while (opcao !== "3");
