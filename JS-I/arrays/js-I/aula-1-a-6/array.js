// Estruturas de dados servem para trabalharmos de forma mais eficiente

// O que da para fazer com os arrays?
  // - Enfileirar e desenfileirar
  // - Empilhar e desempilhar
  // - Achar o indice de um valor
  // - Cortar e concatenar
  // - Entre outras coisas...


// ************************************************************************************************************************************//

// Apresentando o array - Aula 01

const listaDeCompras = [];

listaDeCompras[0] = "Teste";
listaDeCompras[1] = "Outro";
listaDeCompras[2] = 7;

// ************************************************************************************************************************************//

console.log("// ************************************************************* Part 01 - arrays*******************************************************************//");

// Trabalhando com arrays part. 01 - Aula 02

const arr = ["Frodo", "Sam", "Merry", "Pippin", "Gandalf", "Aragorn", "Legolas", "Gimli"]

// Adiconar elementos com - push (adiciona elemento no final do array) ** Ele tambem guarda o novo tamanho do array
arr.push("Emanuel")
console.log("Push - adicionando elemento no final do array", arr);

// Adiconar elementos com - unshift (adiciona elemento no inicio do array) ** Ele tambem guarda o novo tamanho do array
arr.unshift("Amanda")
console.log("Unshift - adicionando elemento no inicio do array", arr);

// Remover elementos com - pop (remove um elemento no FINAL do array) ** Ele tambem devolve o elemento que foi removido
arr.pop()
console.log("Pop - removendo elemento no final do array", arr);

// Remover elementos com - shift (remove um elemento no INCIO do array) ** Ele tambem devolve o elemento que foi removido
let valor = arr.shift();
console.log("Shift - removendo elemento no inicio do array", arr, "removeu: ", valor);

// Pesquisar por um elemento com - includes (verifica se um elemento existe no array)
const inclui = arr.includes("Frodo")
console.log("Includes - verificando se o elemento existe no array (true/false)", inclui);

// Pesquisar por um elemento e devolvendo o indice dele - indexOf (verifica o indice do elemento)
const indice = arr.indexOf("Frodo")
console.log("IndexOf - retorna o indice do elemento e se ele existe (caso retorno seja -1 ele n existe)", indice);

// Cortando o array com - slice(comeco que quer q corte, e ate onde quer q corte)
const hobbits = arr.slice(0, 4);
const outros = arr.slice(-4)
console.log("Slice - cortando um array", hobbits);
console.log("Slice - quatro elementos de tras para frente", outros);

// ************************************************************************************************************************************//

console.log("// ************************************************************* Part 02 - arrays*******************************************************************//");

// Trabalhando com arrays part. 02 - Aula 03

// Concatenando o array com - concat()
const sociedade = hobbits.concat(outros, "Emanuel", "Amanda");
console.log("Concat - pegando o array hobbits e concatenando com o array outros e colocando mais informações no array", sociedade);

// Substituição de elementos com - splice(remove um grupo de elementos do array, e substitui esse grupo por outros elementos)
const elementosRemovidos = sociedade.splice(indice, 1, "Adicionado")
console.log("Splice - remove um grupo de elementos do array, e substitui esse grupo por outros elementos", sociedade, "elemento removido: ", elementosRemovidos);

// Iterar sobre os elementos do array
console.log("Iterando sobre um array");
for (let index = 0; index < sociedade.length; index++) {
  const elemento = sociedade[index]
  console.log(elemento , "se encontra na posicao", index);
}