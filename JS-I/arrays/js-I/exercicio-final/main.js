const vagas = [];

// const vagas = [
//   { nome: "Desenvolvedor Front-end", descricao: "teste", dataLimite: "11/01/2002", candidatos: ["Alice", "Bob", "Charlie"] },
//   { nome: "Analista de Dados", descricao: "teste", dataLimite: "11/01/2002", candidatos: ["David", "Eva"] },
//   { nome: "Designer UX", descricao: "teste", dataLimite: "11/01/2002", candidatos: ["Frank"] }
// ];


function menu() {
  const opcao = prompt(
    `Bem vindo ao cadastro de vagas de emprego!!!!
    1 - Listar vagas dispponíveis
    2 - Criar uma nova vaga
    3 - Visualizar uma vaga
    4 - Inscrever um candidato em uma vaga
    5 - Excluir uma vaga
    6 - Sair`
  );

  return opcao;
}

function listarVagas() {
  const vagaTexto = vagas.reduce((textoFinal, vaga, index) => {
    textoFinal += index + ". "
    textoFinal += vaga.nome
    textoFinal += "( " + vaga.candidatos.length + " candidatos )"
    return textoFinal;
  }, "");

  alert(vagaTexto);
}

function novaVaga() {
  const nome = prompt("Insira um nome para a vaga");
  const descricao = prompt("Insira uma descrição para a vaga");
  const dataLimite = prompt("Insira uma data limite para a vaga");

  const confirma = confirm(
    `Deseja realmente cadastrar esta vaga?
    Nome: ${nome}
    Descrição: ${descricao},
    Data limite: ${dataLimite}`
  )

  if (confirma) {
    const novaVaga = { nome, descricao, dataLimite, candidatos: [] };
    vagas.push(novaVaga);
    alert("Vaga cadastrada com sucesso")
  } else {
    alert(`A vaga ${nome} não foi cadastrada`)
  }
}

function visualizarVaga() {
  const indice = prompt("Informe o indice da vaga que deseja visualizar")
  const vaga = vagas[indice]

  const candidatosEmTexto = vaga.candidatos.reduce((textoFinal, candidato) => {
    return textoFinal + "\n - " + candidato;
  }, "");

  alert(
    `
    Indice ${indice}
    Nome vaga: ${vaga.nome}
    Descrição vaga: ${vaga.descricao}
    Data limite vaga: ${vaga.dataLimite}
    Quantidade de candidatos: ${vaga.candidatos.length}
    Candidatos: ${candidatosEmTexto}
    `
  )
  return vaga
}

function inscreverCandidato() {
  const nomeCandidato = prompt("Insira o nome do candidato(a)")
  const indice = prompt("Informe o indice da vaga que deseja inscrever o (a) candidato(a)")
  const vaga = vagas[indice]

  const confirma = confirm(
    `
    Deseja realmente inscrever este candidato na vaga ${vaga.nome}?
    `
  )

  if (confirma) {
    vaga.candidatos.push(nomeCandidato)
    alert("Candidato inscrito com sucesso")
  } else {
    alert("Algo deu errado, tente novamente")
  }
}

function excluirVaga() {
  const indice = prompt("Informe o indice da vaga que deseja excluir")
  const vaga = vagas[indice]

  const confirma = confirm(
    `
    Deseja realmente excluir a vaga:
    Nome: ${vaga.nome},
    Descrição: ${vaga.descricao},
    Data limite: ${vaga.dataLimite},
    `
  )

  if (confirma) {
    vagas.splice(indice, 1)
    alert("Vaga excluida com sucesso")
  } else {
    alert("Algo deu errado, tente novamente")
  }
}

function executar() {
  let opcao = ""
  do {
    opcao = menu()
    switch (opcao) {
      case "1":
        listarVagas()
        break;
      case "2":
        novaVaga()
        break;
      case "3":
        visualizarVaga();
        break;
      case "4":
        inscreverCandidato();
        break;
      case "5":
        excluirVaga();
        break;
      case "6":
        alert("Saindo do sistema, até logo");
        break;
      default:
        alert("Opção inválida!")
        break;
    }
  } while (opcao !== "6");
}

executar();