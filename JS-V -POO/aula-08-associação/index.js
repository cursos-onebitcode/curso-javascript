const Address = require("./Address");
const Person = require("./Person");

const addr = new Address("Rua Jose Ramao", 110, "Chacara Cachoeira", "Campo Grande", "MS")
const emanuel = new Person("Emanuel Barros", addr) //associação de person com address

console.log(emanuel);
console.log(emanuel.address.fullAddress());