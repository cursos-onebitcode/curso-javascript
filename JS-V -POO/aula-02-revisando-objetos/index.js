function Book(title, pages, tags, author) { // Função construtora -> PascalCase -> Construindo objeto
  this.title = title
  this.pages = pages
  this.tags = tags
  this.author = author
  this.published = false
  this.inStock = 0
  this.addOnStock = (quantity) => {
    this.inStock += quantity
  }
  this.save = () => {
    //salva no banco de dados
  }
}

const author = { name: "Emanuel barros" }
const tags = ["fantasia", "aventura"]
// chamando a função construtora
const eragon = new Book("Eragon", 468, tags, author)
console.log(eragon);

const eldest = new Book("eldest", 500, tags, author)
console.log(eldest);