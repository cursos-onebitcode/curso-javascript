O que é a Programação orientada a objetos?
  - Paradigma de Programação:
    - Conjunto de conceitos e recursos de uma LP
  - Tem como peça central os objetos e a forma de como eles se comunicam
  - Objetos podem conter dados e códigos
  - Classes:
    - São como fábricas de objetos a partir de uma "forma" => (São quase as funções construtoras)
  - Instâncias:
    - São objetos fabricados pelas classes