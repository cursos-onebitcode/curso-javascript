class Account {
  #password //encapsulando
  #balance = 0 //encapsulando
  constructor(user) {
    this.email = user.email
    this.#password = user.password //encapsulando
    // this.#balance = 0 //encapsulando
  }

  getBalance(email, password) {
    if (this.#authenticate(email, password)) { //abstração
      return this.#balance
    } else {
      return null
    }
  }

  #authenticate(email, password) { //abstração
    return this.email === email && this.#password === password
  }
}

const user = {
  email: "emanuel@gmail.com",
  password: 123456
}

const myAccount = new Account(user)
console.log(myAccount);
console.log(myAccount.getBalance("emanuel@gmail.com", 123456));

myAccount.password = 124324234 // nao é a mesma coisa que #password


