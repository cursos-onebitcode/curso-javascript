// Criando uma classe Book
class Book {
  // metodo construtor => adicionar atributos nela
  constructor(title) {
    this.title = title
    this.published = false
  }

  publish() {
    this.published = true
  }
}

const eragon = new Book("Titulo") // instancia da classe Book
eragon.publish()
console.log(eragon);

console.log(eragon instanceof Book); // instanceof => verifica se a instancia é da classe Book, devolve true ou false