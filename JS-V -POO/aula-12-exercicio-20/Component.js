export class Component {
  #element = null //referencia ao elemento do DOM
  constructor(tag, parent, option) {
    this.tag = tag
    this.parent = parent
    this.option = option
    this.build()
  }

  getElement() {
    return this.#element
  }

  build() { // cria o elemento
    this.#element = document.createElement(this.tag)
    Object.assign(this.#element, this.option) // pega o objeto global e joga dentro do elemento
    return this // retorna a propria instancia, podendo retornar outros metodos
  }

  render() {
    //renderizar o elemento na tela (Poderá ser chamado na instancia a qualquer momento)
    if (this.parent instanceof Component) {
      this.parent.getElement().append(this.#element)
    } else {
      document.querySelector(this.parent).append(this.#element)
    }
  }
}