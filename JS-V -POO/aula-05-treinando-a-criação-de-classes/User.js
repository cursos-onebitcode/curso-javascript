class User {
  constructor(fullname, email, password) {
    this.fullname = fullname
    this.email = email
    this.password = password
  }

  login(email, senha) {
    if (email === this.email && senha === this.password) {
      console.log("Login feito com sucesso!!!");
    } else {
      console.log("Usuário ou senha incorretos!!!");
    }
  }
}

const usuario = new User("Emanuel", "teste@gmail.com", 1234)
console.log(usuario);
usuario.login("teste@gmail.com", 1234)
usuario.login("teste@gmail.com", 12345)